package ru.test.test;

import java.io.File;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.test.mypackage.FirstClass;
import ru.test.mypackage.SecondClass;

@RunWith(Arquillian.class)
public class SecondClassTest {

	 @Deployment
	    public static Archive<?> createTestArchive() {
	    	/*
	        return ShrinkWrap.create(WebArchive.class, "test.war")
	                .addClasses(Member.class, MemberRegistration.class, Resources.class)
	                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
	                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
	                // Deploy our test datasource
	                .addAsWebInfResource("test-ds.xml");
	                */
	    	return ShrinkWrap.createFromZipFile(WebArchive.class, new File("target/test.war"));
	    }
	 
	 @Inject
	 private SecondClass cl;
	 
	 @Test
	 public void test(){
		 //SecondClass cl = new SecondClass();
		 System.out.println("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT" + cl.hashCode());
		 System.out.println(cl.init());
		 //SecondClass secondClass = new SecondClass();
		 //secondClass.init();
	 }
}
