package ru.test.mypackage;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

@RequestScoped
public class FirstClass {

	@PostConstruct
	private void init(){
		System.out.println("Создание экземпляря: " + this.getClass().getName());
	}
	
	public String getStr(){
		return this.getClass().getName();
	}
}
